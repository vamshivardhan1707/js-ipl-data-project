const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const matchesPath = path.join(__dirname,'..','data','matches.csv');
const deliveriesPath = path.join(__dirname,'..','data','deliveries.csv');

fs.readFile(matchesPath, 'utf8', (err,data1) => {
    if (err) {
        console.error('Error in reading file:', err);
    };
    const parsedDataOfMatches = papaparse.parse(data1, {
        header: true,
    });

    fs.readFile(deliveriesPath, 'utf8', (err,data2) => {
        if (err) {
            console.error('Error in reading file:', err);
        };
        const parsedDataOfDeliveries = papaparse.parse(data2, {
            header: true,
        });
    
        let matchesData = parsedDataOfMatches.data;
        let deliveriesData = parsedDataOfDeliveries.data;

        function calculateStrikeRates(matchesData, deliveriesData) {
            let years = {};
            for (let match of matchesData) {
                years[match['id']] = match['season'];
            }
        
            let batsmanData = {};
        
            for (let delivery of deliveriesData) {
                const batsman = delivery['batsman'];
                const runs = parseInt(delivery['batsman_runs']);
                const notExtraBall = delivery['wide_runs'] === '0' && delivery['noball_runs'] === '0';
                const year = years[delivery['match_id']];
        
                if (year && batsman) {
                    if (batsman in batsmanData) {
                        if (batsmanData[batsman][year]) {
                            batsmanData[batsman][year].runs += runs;
                            batsmanData[batsman][year].balls += notExtraBall ? 1 : 0;
                        } else {
                            batsmanData[batsman][year] = {
                                runs,
                                balls: notExtraBall ? 1 : 0,
                            };
                        };
                    } else {
                        batsmanData[batsman] = {
                            [year]: {
                                runs,
                                balls: notExtraBall ? 1 : 0,
                            }
                        };
                    };
                };
            };
        
            let strikeRateData = {};
            for (let batsman in batsmanData) {
                strikeRateData[batsman] = {};
                for (let year in batsmanData[batsman]) {
                    const { runs, balls } = batsmanData[batsman][year];
                    const strikeRate = (runs / balls) * 100;
                    strikeRateData[batsman][year] = strikeRate;
                };
            };
        
            return strikeRateData;
        };
        
        let result = calculateStrikeRates(matchesData, deliveriesData);
        
        const jsonData = JSON.stringify(result);
        const outputPath = path.join(__dirname,'..','public','output','strikeRateOfBatsmanForEachSeason.json');

        fs.writeFile(outputPath,jsonData,'utf8',(err) => {
            if (err) {
                console.error('Error writing the file:', err);
            };
            console.log('Saved in output file');
        });
    });
});