const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','deliveries.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.error('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const deliveries = parsedData.data;

    function bowlerWithBestEconomy(deliveries) {
        let bowlers = {};
        for (let delivery of deliveries) {
            if (delivery['is_super_over'] == 1) {
                if (delivery['bowler'] in bowlers) {
                    bowlers[delivery['bowler']][0] += parseInt(delivery['total_runs']); 
                    bowlers[delivery['bowler']][1] += 1;
                } else {
                    bowlers[delivery['bowler']] = [parseInt(delivery['total_runs']),1]
                };
            };
        };
        
        let economyRateData = [];
        for (let bowler in bowlers) {
            let [runs, balls] = bowlers[bowler];
            let economyRate = runs / (balls/6);
            economyRateData.push({ bowler,economyRate });
        };
        economyRateData.sort((a,b) => {
            a.economyRate - b.economyRate;
        });
        
        return economyRateData[0]; 
    };

    let result = bowlerWithBestEconomy(deliveries);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','bowlerWithBestEconomyinSuperOvers.json');

    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        };
        console.log('Saved in output file');
    });   
});
