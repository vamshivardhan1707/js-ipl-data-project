const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','matches.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.error('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const matches = parsedData.data;
    
    function getMatchesPerYear(matches) {
        const matchesPerYear = matches.reduce((seasons,match) => {
            let season = match['season'];
            if (season !== undefined) {
                if (season in seasons) {
                    seasons[season] += 1;
                } else {
                    seasons[season] = 1;
                };
            };
            return seasons;
        },{});
        return matchesPerYear;
    };

    let result = getMatchesPerYear(matches);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','matchesPerYear.json')
    
    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        }
        console.log('Saved in output file');
    });
});
