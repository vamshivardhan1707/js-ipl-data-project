const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const matchesPath = path.join(__dirname,'..','data','matches.csv');
const deliveriesPath = path.join(__dirname,'..','data','deliveries.csv');

fs.readFile(matchesPath, 'utf8', (err,data1) => {
    if (err) {
        console.error('Error in reading file:', err);
    };
    const parsedDataOfMatches = papaparse.parse(data1, {
        header: true,
    });

    fs.readFile(deliveriesPath, 'utf8', (err,data2) => {
        if (err) {
            console.error('Error in reading file:', err);
        };
        const parsedDataOfDeliveries = papaparse.parse(data2, {
            header: true,
        });
    
        let matchesData = parsedDataOfMatches.data;
        let deliveriesData = parsedDataOfDeliveries.data;

        function getTop10EconomicalBowlersIn2015(matchesData,deliveriesData) {
            const idsOf2015 = [];
            const economicalBowlers = {};
            for (let match of matchesData) {
                if (match['season'] === '2015') {
                    idsOf2015.push(match['id']);
                };
            };

            for (let delivery of deliveriesData) {
                if (idsOf2015.includes(delivery['match_id'])) {
                    const bowler = delivery['bowler'];
                    const totalRuns = parseInt(delivery['total_runs']);
        
                    if (economicalBowlers[bowler]) {
                        economicalBowlers[bowler][0] += totalRuns;
                        economicalBowlers[bowler][1] += 1;
                    } else {
                        economicalBowlers[bowler] = [totalRuns, 1];
                    };
                };
            };
        
            const economyRateData = [];
            for (let bowler in economicalBowlers) {
                let [totalRuns, totalBalls] = economicalBowlers[bowler];
                let economyRate = totalRuns / Math.floor(totalBalls/6);
                economyRateData.push({ bowler, economyRate });
            };
        
            economyRateData.sort((a, b) => {
                a.economyRate - b.economyRate;
            });
            
            return economyRateData.slice(0, 10);
        };

        let result = getTop10EconomicalBowlersIn2015(matchesData,deliveriesData);
        
        const jsonData = JSON.stringify(result);
        const outputPath = path.join(__dirname,'..','public','output','top10EconomicalBowlers.json');

        fs.writeFile(outputPath,jsonData,'utf8',(err) => {
            if (err) {
                console.error('Error writing the file:', err);
            };
            console.log('Saved in output file');
        });
    });
});
