const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','matches.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.log('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const matches = parsedData.data;

    function getMatchesWonPerTeamPerYear(matches) {
        const matchesWonPerTeamPerYear = matches.reduce((seasons,match) => {
            let winner = match['winner'];
            let season = match['season'];

            if (season !== undefined) {
                if (season in seasons) {
                    if (winner in seasons[season]) {
                        seasons[season][winner] += 1;
                    } else {
                        seasons[season][winner] = 1;
                    }
                } else {
                    seasons[season] = {};
                };
            };

            return seasons
        },{});

        return matchesWonPerTeamPerYear;
    };

    let result = getMatchesWonPerTeamPerYear(matches);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','matchesWonPerTeamPerYear.json');

    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        } 
        console.log('Saved in output file');
    });
});