const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const matchesPath = path.join(__dirname,'..','data','matches.csv');
const deliveriesPath = path.join(__dirname,'..','data','deliveries.csv');

fs.readFile(matchesPath, 'utf8', (err,data1) => {
    if (err) {
        console.error('Error in reading file:', err);
    };
    const parsedDataOfMatches = papaparse.parse(data1, {
        header: true,
    });

    fs.readFile(deliveriesPath, 'utf8', (err,data2) => {
        if (err) {
            console.error('Error in reading file:', err);
        };
        const parsedDataOfDeliveries = papaparse.parse(data2, {
            header: true,
        });
    
        let matchesData = parsedDataOfMatches.data;
        let deliveriesData = parsedDataOfDeliveries.data;
    
        function getExtraRunsPerTeamIn2016(matchesData,deliveriesData) {
            const matchesOf2016 = matchesData.filter((match) => {
                return match['season'] === '2016';
            });

            const idsOf2016 = matchesOf2016.map((match) => {
                return match['id'];
            });

            const extraRunsPerTeamIn2016 = deliveriesData.reduce((teams,delivery) => {
                if (idsOf2016.includes(delivery['match_id'])) {
                    if (delivery['bowling_team'] in teams) {
                        teams[delivery['bowling_team']] += parseInt(delivery['extra_runs']);
                    } else {
                        teams[delivery['bowling_team']] = parseInt(delivery['extra_runs']);
                    };
                };
                return teams;
            },{})

            return extraRunsPerTeamIn2016;
        };

        let result = getExtraRunsPerTeamIn2016(matchesData,deliveriesData);

        const jsonData = JSON.stringify(result);
        const outputPath = path.join(__dirname,'..','public','output','extraRunsPerTeam.json');

        fs.writeFile(outputPath,jsonData,'utf8',(err) => {
            if (err) {
                console.error('Error writing the file:', err);
            } 
            console.log('Saved in output file');
        });
    });
});

