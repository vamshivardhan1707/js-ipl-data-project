const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','deliveries.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.error('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const deliveries = parsedData.data;

    function maxDismissalsByBowler(deliveries) {
        let dismissalCounts = {};
    
        for (let delivery of deliveries) {
            const batsman = delivery['batsman'];
            const dismissalKind = delivery['dismissal_kind'];
            const bowler = delivery['bowler'];
    
            if (dismissalKind && dismissalKind !== 'run out') {
                if (!(batsman in dismissalCounts)) {
                    dismissalCounts[batsman] = {};
                };
    
                if (bowler in dismissalCounts[batsman]) {
                    dismissalCounts[batsman][bowler] += 1;
                } else {
                    dismissalCounts[batsman][bowler] = 1;
                };
            };
        };
    
        let maxBatsman = '';
        let maxDismissals = 0;
        let maxBowler = '';
    
        for (let batsman in dismissalCounts) {
            for (let bowler in dismissalCounts[batsman]) {
                if (dismissalCounts[batsman][bowler] > maxDismissals) {
                    maxBatsman = batsman;
                    maxDismissals = dismissalCounts[batsman][bowler];
                    maxBowler = bowler;
                };
            };
        };
    
        return { maxBatsman, maxBowler, maxDismissals };
    };
    
    let result = maxDismissalsByBowler(deliveries);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','highestNoOfTimesDismissedByPlayer.json');

    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        };
        console.log('Saved in output file');
    });
});