const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','matches.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.error('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const matches = parsedData.data;

    function getHighestPlayerOfMatchAwards(matches) {
        let years = {};

        for (let match of matches) {
            if (!years[match['season']]) {
                years[match['season']] = {};
            };
    
            if (match['player_of_match'] in years[match['season']]) {
                years[match['season']][match['player_of_match']] += 1;
            } else {
                years[match['season']][match['player_of_match']] = 1;
            };
        };
    
        let maxAwards = {};

        for (let year in years) {
            if (year !== 'undefined') {
                const awards = years[year];
                let maxPlayer = '';
                let maxCount = 0;
                for (let player in awards) {
                    if (awards[player] > maxCount) {
                        maxPlayer = player;
                        maxCount = awards[player];
                    };
                };
                maxAwards[year] = maxPlayer;
            };
        };

        return maxAwards;
    };

    let result = getHighestPlayerOfMatchAwards(matches);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','highestPlayerOfMatchAwardsPerSeason.json')
    
    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        }
        console.log('Saved in output file');
    });
});