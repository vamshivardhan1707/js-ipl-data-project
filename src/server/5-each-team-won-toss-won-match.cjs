const fs = require('fs');
const papaparse = require('papaparse');
const path = require('path');

const inputPath = path.join(__dirname,'..','data','matches.csv');

fs.readFile(inputPath, 'utf8', (err,data) => {
    if (err) {
        console.error('Error reading the file:', err);
    };
    const parsedData = papaparse.parse(data, {
        header: true,
    });

    const matches = parsedData.data;

    function getEachTeamWonTossWonMatch(matches) {
        let teams = {};
        for (let match of matches) {
            if (match['toss_winner'] === match['winner'] && match['winner'] !== undefined) {
                
                if (match['winner'] in teams) {
                    teams[match['winner']] += 1; 
                } else {
                    teams[match['winner']] = 1;
                };
            };
        };
        return teams
    };

    let result = getEachTeamWonTossWonMatch(matches);
    
    const jsonData = JSON.stringify(result);
    const outputPath = path.join(__dirname,'..','public','output','eachTeamWonTossWonMatch.json')
    
    fs.writeFile(outputPath,jsonData,'utf8',(err) => {
        if (err) {
            console.error('Error writing the file:', err);
        }
        console.log('Saved in output file');
    });
});